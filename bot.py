import  tweepy
from datetime import date
from scrap import Scrap

# Mettre ici les clés d'authentification twitter (developer.twitter.com)
consumer_key = '******'
consumer_secret = '*******'
access_token = '**********'
access_token_secret = '***************'

# Authentification
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

# On utilise la classe Scrap de l'autre fichier
mois = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre']
today = date.today()
sc = Scrap('https://anniversaire-celebrite.com/date/', today.strftime("%d"), mois[today.month - 1])

# Post
api.update_status('Joyeux Anniversaire ' + sc.getRandomPersonnalite() + ' !! 🎉🎈🎈') 