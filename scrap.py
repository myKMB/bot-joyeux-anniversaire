# Imports
import bs4
import requests
import re
from datetime import date
from random import seed
from random import randint

class Scrap:

    url = ''
    jour = ''
    mois = ''

    def __init__(self, url, jour, mois):
        self.url = url
        self.jour = jour
        self.mois = mois

    def getRandomPersonnalite(self):
        page = self.url + self.jour + '-' + self.mois

        # On récupère la page
        response = requests.get(page)
        soup = bs4.BeautifulSoup(response.text, 'html.parser')

        # On récupère la balise image (une img par personnalité)
        img_box = soup.findAll('img', {"src": re.compile('^u')})

        parameters = ['title']

        for par in parameters:
            personnalite = []
            for el in img_box:
                j = el[par]
                personnalite.append(j)

        size = len(personnalite)

        # On choisit au hasard une des personnalité
        seed(1)
        return personnalite[randint(0, size)]